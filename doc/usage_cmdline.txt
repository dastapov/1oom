################
# 1oom_cmdline #
################

1oom_cmdline is a proof of concept textual UI.

See doc/usage_common.txt for the data/user file locations and command-line
arguments.

The UI is still missing many pieces. Type '?' for help.
